//
//  PhotocardViewModel.swift
//  K-Collect
//
//  Created by Fransiscus Verrol Yaurentius on 23/08/21.
//

import Foundation

class PhotocardViewModel: ObservableObject{
    
    @Published var photocardList: [Photocard] = [Photocard]()
    
    func getData(){
        photocardList = CoreDataService.shared.fetchPhotocards()
    }
    
    func sendDataToWatch(photocard: [Photocard]){
        var dataForWatch: [String] = [String]()
        for pc in photocard {
            if let data = pc.name{
                dataForWatch.append(data)
            }
        }
        ConnectivityProvider.shared.session.sendMessage(["photocardList" : dataForWatch], replyHandler: nil){ (error) in
            print(error)
        }
    }
    
}
