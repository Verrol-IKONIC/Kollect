//
//  PhotocardDetailsViewModel.swift
//  K-Collect
//
//  Created by Fransiscus Verrol Yaurentius on 24/08/21.
//

import Foundation

class PhotocardDetailsViewModel: ObservableObject{
    
    @Published var photocard: Photocard = Photocard()
    
    func delete(photocard: Photocard){
        CoreDataService.shared.delete(photocard: photocard)
    }
}
