//
//  EditPhotocardViewModel.swift
//  K-Collect
//
//  Created by Fransiscus Verrol Yaurentius on 25/08/21.
//

import Foundation
import SwiftUI

class EditPhotocardViewModel: ObservableObject{
    
    var photocard: Photocard = Photocard()
    
    @Published var name: String = ""
    @Published var album: String = ""
    @Published var buyPrice: String = ""
    @Published var tradeMember: String = ""
    @Published var salePrice: String = ""
    
    @Published var selectedGetOption = GetOptions.buy
    @Published var selectedTradeOption = BoolOptions.yes
    @Published var selectedSaleOption = BoolOptions.yes
    
    @Published var image: Image?
    @Published var inputImage: UIImage?
    
    enum GetOptions: String, CaseIterable{
        case buy = "Buy"
        case pull = "Pull"
    }
    
    enum BoolOptions: String, CaseIterable{
        case yes = "Yes"
        case no = "No"
    }
    
    func loadImage(){
        guard let inputImage = inputImage else { return }
        image = Image(uiImage: inputImage)
    }
    
    func validateForm() -> Bool {
        guard inputImage != nil else {return true}
        guard !name.isEmpty else { return true }
        guard !album.isEmpty else { return true }
        if selectedGetOption == GetOptions.buy{
            guard !buyPrice.isEmpty else { return true }
        }
        if selectedTradeOption == BoolOptions.yes{
            guard !tradeMember.isEmpty else { return true }
        }
        if selectedSaleOption == BoolOptions.yes{
            guard !salePrice.isEmpty else { return true }
        }
        return false
    }
    
    func getPhotocardData(photocard: Photocard){
        inputImage = UIImage(data: photocard.image ?? Data())
        name = photocard.name ?? ""
        album = photocard.album ?? ""
        if photocard.buy == GetOptions.buy.rawValue{
            selectedGetOption = GetOptions.buy
        }else{
            selectedGetOption = GetOptions.pull
        }
        if photocard.buyPrice != 0{
            buyPrice = String(photocard.buyPrice)
        }
        if photocard.tradeable == BoolOptions.yes.rawValue{
            selectedTradeOption = BoolOptions.yes
        }else{
            selectedTradeOption = BoolOptions.no
        }
        tradeMember = photocard.tradeMember ?? ""
        if photocard.sellable == BoolOptions.yes.rawValue{
            selectedSaleOption = BoolOptions.yes
        }else{
            selectedSaleOption = BoolOptions.no
        }
        if photocard.sellPrice != 0{
            salePrice = String(photocard.sellPrice)
        }
    }
    
    func updatePhotocard(photocard: Photocard){
        photocard.image = inputImage?.jpegData(compressionQuality: 1.0)
        photocard.name = name
        photocard.album = album
        photocard.buy = selectedGetOption.rawValue
        photocard.buyPrice = Double(buyPrice) ?? 0
        photocard.tradeable = selectedTradeOption.rawValue
        photocard.tradeMember = tradeMember
        photocard.sellable = selectedSaleOption.rawValue
        photocard.sellPrice = Double(salePrice) ?? 0
        
        CoreDataService.shared.save()
    }
    
}
