//
//  K_CollectApp.swift
//  K-Collect
//
//  Created by Fransiscus Verrol Yaurentius on 16/08/21.
//

import SwiftUI

@main
struct K_CollectApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
