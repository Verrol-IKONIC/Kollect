//
//  ConnectivityProvider.swift
//  K-Collect
//
//  Created by Fransiscus Verrol Yaurentius on 25/08/21.
//

import Foundation
import WatchConnectivity

class ConnectivityProvider: NSObject, WCSessionDelegate {
    
    static let shared = ConnectivityProvider()
    
    var session: WCSession
        init(session: WCSession = .default){
            self.session = session
            super.init()
            self.session.delegate = self
            session.activate()
        }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?){
    }
    func sessionDidBecomeInactive(_ session: WCSession){}
    func sessionDidDeactivate(_ session: WCSession){}
    
}
