//
//  CoreDataService.swift
//  K-Collect
//
//  Created by Fransiscus Verrol Yaurentius on 23/08/21.
//

import Foundation
import CoreData

class CoreDataService {
    
    let container: NSPersistentContainer
    static let shared = CoreDataService()
        
        var viewContext: NSManagedObjectContext {
            return container.viewContext
        }
    
    private init(){
        container = NSPersistentContainer(name: "Kollect")
        container.loadPersistentStores{(description, error) in
            if let error = error {
                print("ERROR LOADING CORE DATA. \(error)")
            }
        }
    }
    
    func fetchPhotocards() -> [Photocard]{
        let request = NSFetchRequest<Photocard>(entityName: "Photocard")
        
        do{
            return try container.viewContext.fetch(request)
        } catch let error{
            print("Error fetching. \(error)")
        }
        return []
    }
    
    func delete(photocard: Photocard){
        container.viewContext.delete(photocard)
        save()
    }
    
    func save(){
        do{
            try container.viewContext.save()
        } catch let error{
            print("Error saving. \(error)")
        }
        
    }
    
}
