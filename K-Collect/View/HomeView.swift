//
//  HomeView.swift
//  K-Collect
//
//  Created by Fransiscus Verrol Yaurentius on 20/08/21.
//

import SwiftUI

struct HomeView: View {
    
    @State var chosenView: Options = .photocard
    
    
    var body: some View {
        NavigationView{
            VStack{
//                Picker("Options", selection: $chosenView){
//                    ForEach(Options.allCases, id: \.self){
//                        Text($0.rawValue)
//                    }
//                }
//                .pickerStyle(SegmentedPickerStyle())
//                .padding()
//                .padding(.horizontal, 10)
                Spacer()
                ChosenOptionView(selectedMenu: chosenView)
                Spacer()
            }.navigationBarTitle("Photocards", displayMode: .large)
        }    }
}

enum Options: String, CaseIterable{
    case photocard = "Photocard"
    case album = "Album"
    case lightstick = "Lightstick"
}

struct ChosenOptionView: View {
    
    var selectedMenu: Options
    
    var body: some View{
        switch selectedMenu {
        case .photocard:
            VStack{
                PhotocardView()
            }
        case .album:
            VStack{
                Text("Feature Coming Soon")
                    .font(.title)
            }
            
        case .lightstick:
            VStack{
                Text("Feature Coming Soon")
                    .font(.title)
            }
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
