//
//  PhotocardDetailsView.swift
//  K-Collect
//
//  Created by Fransiscus Verrol Yaurentius on 20/08/21.
//

import SwiftUI

struct PhotocardDetailsView: View {
    
    @Environment(\.presentationMode) var presentationMode
    
    @StateObject private var viewModel: PhotocardDetailsViewModel = PhotocardDetailsViewModel()
    
    @State var photocard: Photocard
    
    @State private var isEditing: Bool = true
    
    @State private var deleteAlertIsPresented = false
    
    @State private var isPresented = false
    
    var body: some View {
//        NavigationView{
            VStack{
                if let photocardImage = photocard.image{
                    Image(uiImage: UIImage(data: photocardImage) ?? UIImage())
                        .resizable()
                        .frame(width: 250, height: 380)
                        .cornerRadius(10)
                }
                Text(photocard.name ?? "")
                    .font(.largeTitle)
                    .foregroundColor(Color("MainCoral"))
                    .padding(.top, 30)
                if photocard.buy == "Buy"{
                    HStack{
                        Text(photocard.album ?? "").font(.subheadline)
                            .foregroundColor(Color("MainGreen"))
                            .padding(.leading, -4)
                    }.padding(.top, 10)
                    HStack{
                        Text("Bought").font(.subheadline).bold()
                        Text("at").font(.subheadline).padding(.leading, -4)
                        Text("IDR \(NumberFormatter.localizedString(from: NSNumber(value: photocard.buyPrice), number: .decimal))").font(.subheadline).bold().padding(.leading, -4)
                    }.padding(.top, 10)
                }else{
                    HStack{
                        Text("Pulled").font(.subheadline)
                            .foregroundColor(Color("MainGreen"))
                            .bold()
                        Text("from \(photocard.album ?? "")").font(.subheadline)
                            .foregroundColor(Color("MainGreen"))
                            .padding(.leading, -4)
                    }.padding(.top, 10)
                }
                if photocard.tradeable == "Yes"{
                    HStack{
                        Text("For trade").font(.subheadline)
                            .bold()
                            .padding(.top, 3)
                        Text("to").font(.subheadline)
                            .padding(.top, 3).padding(.leading, -4)
                        Text(photocard.tradeMember ?? "").font(.subheadline)
                            .bold()
                            .padding(.top, 3).padding(.leading, -4)
                    }
                }else{
                    Text("Not for trade").font(.subheadline)
                        .bold()
                        .padding(.top, 3)
                }
                if photocard.sellable == "Yes"{
                    HStack{
                        Text("For sale").font(.subheadline)
                            .bold()
                            .padding(.top, 3)
                        Text("at").font(.subheadline)
                            .padding(.top, 3).padding(.leading, -4)
                        Text("IDR \(NumberFormatter.localizedString(from: NSNumber(value: photocard.sellPrice), number: .decimal))").font(.subheadline)
                            .bold()
                            .padding(.top, 3).padding(.leading, -4)
                    }
                }else{
                    Text("Not for sale").font(.subheadline)
                        .bold()
                        .padding(.top, 3)
                }
            }.padding(.bottom, 200)
            .navigationBarTitle("", displayMode: .large)
            .toolbar(content: {
//                ToolbarItemGroup(placement: .navigationBarLeading) {
//                    Button("Close") {
//                        presentationMode.wrappedValue.dismiss()
//                    }
//                }
                ToolbarItemGroup(placement: .navigationBarTrailing) {
                    Menu{
                        Button(action: {
                            isPresented.toggle()
                        }, label: {
                            Label("Edit", systemImage: "pencil").foregroundColor(Color("MainGreen"))
                        })
                        Button(action: {
                            deleteAlertIsPresented.toggle()
                        }, label: {
                            Label("Delete", systemImage: "xmark").foregroundColor(Color("MainCoral"))
                        })
                    }label: {
                        Image(systemName: "ellipsis.circle")
                            .font(.title2)
                    }
                }
            })
            .alert(isPresented: $deleteAlertIsPresented) {
                Alert(
                    title: Text("Are you sure?"),
                    primaryButton: Alert.Button.default(Text("Yes").foregroundColor(Color("MainCoral")), action: {
                        viewModel.delete(photocard: photocard)
                        presentationMode.wrappedValue.dismiss()
                    }),
                    secondaryButton: Alert.Button.default(Text("No"), action: {
                        
                    })
                )
            }
            .fullScreenCover(isPresented: $isPresented){
                EditPhotocardView(photocard: photocard)
                    .onDisappear(){
                        
                    }
            }
//        }
    }
}

struct PhotocardDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        let photocard = Photocard(context: CoreDataService.shared.viewContext)
        PhotocardDetailsView(photocard: photocard)
    }
}
