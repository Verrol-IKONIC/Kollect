//
//  PhotocardView.swift
//  K-Collect
//
//  Created by Fransiscus Verrol Yaurentius on 20/08/21.
//

import SwiftUI

struct PhotocardView: View {
    
    @StateObject private var viewModel: PhotocardViewModel = PhotocardViewModel()
    
    @State private var isPresented = false
    @State private var photocardDetailsIsPresented = false
    
    @State private var isEditing: Bool = false
    
    var body: some View {
        VStack{
            if viewModel.photocardList.isEmpty{
                Text("No photocard").font(.largeTitle)
                    .bold()
            }else{
                GeometryReader{ geometry in
                    ScrollView(viewModel.photocardList.count > 1 ? .horizontal : [], showsIndicators: false){
                        LazyHStack{
                            ForEach(viewModel.photocardList.indices, id: \.self){i in
                                NavigationLink(
                                    destination: PhotocardDetailsView(photocard: viewModel.photocardList[i]),
                                    label: {
                                        VStack(alignment: .center){
                                            if let photocardImage = viewModel.photocardList[i].image{
                                                Image(uiImage: UIImage(data: photocardImage) ?? UIImage())
                                                    .resizable()
                                                    .frame(width: 250, height: 380)
                                                    .cornerRadius(10)
                                            }
                                            Text(viewModel.photocardList[i].name ?? "")
                                                .font(.largeTitle)
                                                .foregroundColor(Color("MainCoral"))
                                                .padding(.top, 30)
                                            Text(viewModel.photocardList[i].album ?? "").font(.subheadline)
                                                .foregroundColor(Color("MainGreen"))
                                                .padding(.top, 10)
                                        }.padding(.leading, i == 0 ? ((geometry.size.width - 250)/2) : 10)
                                        .padding(.trailing, i == (viewModel.photocardList.count - 1) ? ((geometry.size.width - 250)/2) : 0)
                                    }).onDisappear(perform:{
                                        viewModel.getData()
                                        viewModel.sendDataToWatch(photocard: viewModel.photocardList)
                                    })
//                                Button(action: {
//                                    photocardDetailsIsPresented.toggle()
//                                }, label: {
//                                    VStack(alignment: .center){
//                                        if let photocardImage = viewModel.photocardList[i].image{
//                                            Image(uiImage: UIImage(data: photocardImage) ?? UIImage())
//                                                .resizable()
//                                                .frame(width: 250, height: 380)
//                                                .cornerRadius(10)
//                                        }
//                                        Text(viewModel.photocardList[i].name ?? "")
//                                            .font(.largeTitle)
//                                            .foregroundColor(Color("MainCoral"))
//                                            .padding(.top, 30)
//                                        Text(viewModel.photocardList[i].album ?? "").font(.subheadline)
//                                            .foregroundColor(Color("MainGreen"))
//                                            .padding(.top, 10)
//                                    }.padding(.leading, i == 0 ? ((geometry.size.width - 250)/2) : 10)
//                                    .padding(.trailing, i == (viewModel.photocardList.count - 1) ? ((geometry.size.width - 250)/2) : 0)
//                                }).sheet(isPresented: $photocardDetailsIsPresented){
//                                    PhotocardDetailsView(photocard: viewModel.photocardList[i])
//                                }
                            }
                        }
                    }
                }
                if ConnectivityProvider.shared.session.isReachable{
                    Button(action: {viewModel.sendDataToWatch(photocard: viewModel.photocardList)}, label: {
                        RoundedRectangle(cornerRadius: 10).overlay(
                            Text("Send data to watch").font(.title2).bold().foregroundColor(.white)
                        ).frame(width: 250, height: 50)
                    }).padding(.bottom, 50)
                }
            }
        }.onAppear(perform: {
            viewModel.getData()
        })
        .toolbar(content: {
            ToolbarItemGroup(placement: .navigationBarTrailing) {
                //                Button(action: {}, label: {
                //                    Image(systemName: "ellipsis.circle")
                //                })
                Button(action: {
                    isPresented.toggle()
                }, label: {
                    Image(systemName: "plus")
                }).fullScreenCover(isPresented: $isPresented){
                    AddPhotocardView()
                        .onDisappear(perform:{
                            viewModel.getData()
                        })
                }
            }
        })
    }
}

struct PhotocardView_Previews: PreviewProvider {
    static var previews: some View {
        PhotocardView()
    }
}
