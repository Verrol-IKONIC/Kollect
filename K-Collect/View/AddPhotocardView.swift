//
//  AddPhotocardView.swift
//  K-Collect
//
//  Created by Fransiscus Verrol Yaurentius on 20/08/21.
//

import SwiftUI

struct AddPhotocardView: View {
    
    @Environment(\.presentationMode) var presentationMode
    @State var disableDoneButton = true
    
    @StateObject private var viewModel: AddPhotocardViewModel = AddPhotocardViewModel()
    
    @State private var showImagePicker = false
    
    let formatter: NumberFormatter = {
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            return formatter
        }()
    
    var body: some View {
        NavigationView{
            ScrollView{
                    if viewModel.image != nil{
                        viewModel.image?.resizable().frame(width: 150, height: 230).cornerRadius(10).padding(.top, 30)
                    }else{
                        Image("NoImage").resizable().frame(width: 150, height: 230).padding(.top, 30)
                    }
                Button(action: {
                    self.showImagePicker.toggle()
                }, label: {
                    Text("Add image")
                        .foregroundColor(Color("MainGreen"))
                }).fullScreenCover(isPresented: $showImagePicker, onDismiss: viewModel.loadImage) {
                    ImagePicker(image: self.$viewModel.inputImage)
                }.onChange(of: viewModel.image, perform: { value in
                    disableDoneButton = viewModel.validateForm()
                })
                VStack{
                    VStack{
                        HStack{
                            Text("Name").padding(.leading)
                            TextField(
                                "Photocard Name",
                                text: $viewModel.name
                            ).onChange(of: viewModel.name, perform: { value in
                                disableDoneButton = viewModel.validateForm()
                            }).padding(.leading, 55)
                        }
                        Divider()
                        HStack{
                            Text("Album").padding(.leading)
                            TextField(
                                "Album Name",
                                text: $viewModel.album
                            ).onChange(of: viewModel.album, perform: { value in
                                disableDoneButton = viewModel.validateForm()
                            }).padding(.leading, 52)
                        }
                        Divider()
                    }.padding(.horizontal).padding(.top, 30)
                    VStack{
                        HStack{
                            Text("Buy/Pull").padding(.leading)
                            Picker("GetOptions", selection: $viewModel.selectedGetOption){
                                ForEach(AddPhotocardViewModel.GetOptions.allCases, id: \.self){
                                    Text($0.rawValue)
                                }
                            }.pickerStyle(SegmentedPickerStyle())
                            .padding(.leading, 40)
                            .onChange(of: viewModel.selectedGetOption, perform: { value in
                                disableDoneButton = viewModel.validateForm()
                            })
                        }
                        Divider()
                        if viewModel.selectedGetOption == AddPhotocardViewModel.GetOptions.buy{
                            HStack{
                                Text("Price").padding(.leading)
                                TextField(
                                    "Purchase Price",
                                    text: $viewModel.buyPrice
                                ).onChange(of: viewModel.buyPrice, perform: { value in
                                    disableDoneButton = viewModel.validateForm()
                                }).keyboardType(.numberPad)
                                .padding(.leading, 61)
                            }
                            Divider()
                        }
                    }.padding(.horizontal).padding(.top, 30)
                    VStack{
                        HStack{
                            Text("For Trade").padding(.leading)
                            Picker("TradeOptions", selection: $viewModel.selectedTradeOption){
                                ForEach(AddPhotocardViewModel.BoolOptions.allCases, id: \.self){
                                    Text($0.rawValue)
                                }
                            }.pickerStyle(SegmentedPickerStyle())
                            .padding(.leading, 28)
                            .onChange(of: viewModel.selectedTradeOption, perform: { value in
                                disableDoneButton = viewModel.validateForm()
                            })
                        }
                        Divider()
                        if viewModel.selectedTradeOption == AddPhotocardViewModel.BoolOptions.yes{
                            HStack{
                                Text("To").padding(.leading)
                                TextField(
                                    "Which Member",
                                    text: $viewModel.tradeMember
                                ).onChange(of: viewModel.tradeMember, perform: { value in
                                    disableDoneButton = viewModel.validateForm()
                                }).padding(.leading, 80)
                            }
                            Divider()
                        }
                    }.padding(.horizontal).padding(.top, 30)
                    VStack{
                        HStack{
                            Text("For Sale").padding(.leading)
                            Picker("TradeOptions", selection: $viewModel.selectedSaleOption){
                                ForEach(AddPhotocardViewModel.BoolOptions.allCases, id: \.self){
                                    Text($0.rawValue)
                                }
                            }.pickerStyle(SegmentedPickerStyle())
                            .padding(.leading, 38)
                            .onChange(of: viewModel.selectedSaleOption, perform: { value in
                                disableDoneButton = viewModel.validateForm()
                            })
                        }
                        Divider()
                        if viewModel.selectedSaleOption == AddPhotocardViewModel.BoolOptions.yes{
                            HStack{
                                Text("Price").padding(.leading)
                                TextField(
                                    "Selling Price",
                                    text: $viewModel.salePrice
                                ).onChange(of: viewModel.salePrice, perform: { value in
                                    disableDoneButton = viewModel.validateForm()
                                }).keyboardType(.numberPad)
                                .padding(.leading, 61)
                            }
                            Divider()
                        }
                    }.padding(.horizontal).padding(.top, 30)
                }
            }.navigationBarTitle("Add photocard to collection", displayMode: .inline)
            .toolbar(content: {
                ToolbarItemGroup(placement: .navigationBarLeading) {
                    Button("Cancel") {
                        presentationMode.wrappedValue.dismiss()
                    }
                }
                ToolbarItemGroup(placement: .navigationBarTrailing) {
                    Button("Done") {
                        viewModel.addPhotocard()
                        presentationMode.wrappedValue.dismiss()
                    }.disabled(disableDoneButton)
                }
            })
        }
    }
}

struct AddPhotocardView_Previews: PreviewProvider {
    static var previews: some View {
        AddPhotocardView()
    }
}
