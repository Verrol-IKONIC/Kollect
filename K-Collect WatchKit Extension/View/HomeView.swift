//
//  HomeView.swift
//  K-Collect WatchKit Extension
//
//  Created by Fransiscus Verrol Yaurentius on 24/08/21.
//

import SwiftUI

struct HomeView: View {
    var body: some View {
            VStack{
                NavigationLink(
                    destination: PhotocardView(),
                    label: {
                        HStack{
                            Image(systemName: "greetingcard.fill")
                            Text("Photocard")
                        }.padding(10).foregroundColor(Color("MainCoral"))
                    })
//                NavigationLink(
//                    destination: Text("Destination"),
//                    label: {
//                        HStack{
//                            Image(systemName: "books.vertical.fill")
//                            Text("Album")
//                        }.padding(10)
//                    })
//                NavigationLink(
//                    destination: /*@START_MENU_TOKEN@*/Text("Destination")/*@END_MENU_TOKEN@*/,
//                    label: {
//                        HStack{
//                            Image(systemName: "wand.and.stars")
//                            Text("Lightstick")
//                        }.padding(10)
//                    })
            }
            .navigationTitle("Kollect")
            .accentColor(Color("MainCoral"))
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
