//
//  PhotocardView.swift
//  K-Collect WatchKit Extension
//
//  Created by Fransiscus Verrol Yaurentius on 24/08/21.
//

import SwiftUI

struct PhotocardView: View {
    
    @StateObject private var viewModel: WatchPhotocardViewModel = WatchPhotocardViewModel()
    
    @ObservedObject var model = ConnectivityProvider()
    
    var body: some View {
        VStack{
            if viewModel.photocardList.isEmpty{
                Text("No photocard").bold()
            }else{
                GeometryReader{ geometry in
                    ScrollView(viewModel.photocardList.count > 1 ? .vertical : [], showsIndicators: false){
                        LazyVStack{
                            ForEach(viewModel.photocardList.indices, id: \.self){i in
//                                VStack(alignment: .center){
//                                    if let photocardImage = viewModel.photocardList[i]{
//                                        Image(uiImage: UIImage(data: photocardImage) ?? UIImage())
//                                            .resizable()
//                                            .frame(width: 100, height: 150)
//                                            .cornerRadius(10)
//                                    }
//                                }.padding(.leading, i == 0 ? ((geometry.size.width - 100)/2) : 10)
//                                .padding(.trailing, i == (viewModel.photocardList.count - 1) ? ((geometry.size.width - 100)/2) : 0)
                                Text(viewModel.photocardList[i])
                                    .font(.title3)
                                    .padding(.top)
                            }
                        }
                    }
                }
            }
        }.onAppear(perform: {
            viewModel.getData()
        })
        .navigationTitle("Kollect")
    }
}

struct PhotocardView_Previews: PreviewProvider {
    static var previews: some View {
        PhotocardView()
    }
}
