//
//  ConnectivityProvider.swift
//  K-Collect WatchKit Extension
//
//  Created by Fransiscus Verrol Yaurentius on 24/08/21.
//

import Foundation
import WatchConnectivity

class ConnectivityProvider: NSObject, WCSessionDelegate, ObservableObject {
    
    @Published var photocardList: [String] = [String]()
    static let shared = ConnectivityProvider()
    
    var session: WCSession
    init(session: WCSession = .default){
        self.session = session
        super.init()
        self.session.delegate = self
        session.activate()
    }
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("Dapat data photocard")
        DispatchQueue.main.async {
            self.photocardList = message["photocardList"] as? [String] ?? []
        }
    }
}
