//
//  PhotocardViewModel.swift
//  K-Collect WatchKit Extension
//
//  Created by Fransiscus Verrol Yaurentius on 25/08/21.
//

import Foundation

class WatchPhotocardViewModel: ObservableObject{
    
    @Published var photocardList: [String] = [String]()
    
    func getData(){
        self.photocardList = ConnectivityProvider.shared.photocardList
    }
    
}
