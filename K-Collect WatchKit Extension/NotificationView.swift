//
//  NotificationView.swift
//  K-Collect WatchKit Extension
//
//  Created by Fransiscus Verrol Yaurentius on 16/08/21.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
