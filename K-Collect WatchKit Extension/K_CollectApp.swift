//
//  K_CollectApp.swift
//  K-Collect WatchKit Extension
//
//  Created by Fransiscus Verrol Yaurentius on 16/08/21.
//

import SwiftUI

@main
struct K_CollectApp: App {
    @SceneBuilder var body: some Scene {
        WindowGroup {
            NavigationView {
                ContentView()
            }
        }

        WKNotificationScene(controller: NotificationController.self, category: "myCategory")
    }
}
